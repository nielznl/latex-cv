## latex-cv

CV using LaTeX based on [**CV by Jan Vorisek**](https://github.com/janvorisek/minimal-latex-cv).

### Icon reference
[fontawesome5.pdf](https://mirrors.ibiblio.org/CTAN/fonts/fontawesome5/doc/fontawesome5.pdf)

### Preview
[![cv.png](cv.png "CV Image")](cv.pdf)
